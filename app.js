require('dotenv').config()
const express  = require('express')
const cors     = require('cors')
const logger   = require('morgan')
const mongoose = require('mongoose')

mongoose.connect(`mongodb://${process.env.DB_USER}:${process.env.DB_USER_PASSWORD}@ds123372.mlab.com:23372/doabangsa-tv`, { useNewUrlParser: true })

const port = process.env.PORT || 3000

const tagRouter = require('./routes/tag.route')

const app = express()
const db  = mongoose.connection

db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function() {
  console.log('Connect to Database Doa Bangsa TV!')
})

app.use(cors())
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.use('/tags', tagRouter)

app.listen(port, () => {
  console.log(`Listening on Port ${port}`)
})

module.exports = app