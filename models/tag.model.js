const mongoose = require('mongoose')
const Schema = mongoose.Schema

const TagSchema = new Schema({
  tag: {
    type: String
  },
  url: {
    type: String
  }
})

const Tag = mongoose.model('Tag', TagSchema)

module.exports = Tag