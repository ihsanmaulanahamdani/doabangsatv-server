const router = require('express').Router()
const { readTags } = require('../controllers/tag.controller')

router.get('/', readTags)

module.exports = router