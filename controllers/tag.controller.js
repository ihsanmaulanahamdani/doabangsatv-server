const Tag = require('../models/tag.model')

module.exports = {
  readTags: async (req, res) => {
    try {
      const tags = await Tag.find()

      res
        .json({
          code: 200,
          message: 'Read All Tag Success!',
          data: tags
        })
    } catch (error) {
      res
        .json({
          code: 500,
          message: 'Oops! Something Went Wrong.'
        })
    }
  }
}